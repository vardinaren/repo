### Role Based Access Control

This is maven project which can be imported into `IntelliJ` or `Eclipse`.

Use `mvn clean install` for downloading the dependencies. Sample test cases are available in the `test` directory

The Application logic is present in `IRoleBasedApplication` class. This interface consists of the important functions that the application has to perform.

We have two DB accessor (or two services)
1. User Service. (Limited to having email id and general lookups)
2. Role Service. (Consists of mapping between RoleId to Role, Role to latest updated version and Role to User Maps)

Couple of assumptions to note:
1. We pick the latest role in order to check if the user has access to a resource
2. Even if we have one role which says user is eligible for access, we allow access.