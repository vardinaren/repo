package com.locus.role;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.*;
import com.locus.role.repository.IRoleRepository;
import com.locus.role.repository.IUserRepository;
import com.locus.role.repository.RoleRepository;
import com.locus.role.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


public class RoleBasedAccessControlTest {
    private IRoleBasedApplication roleBasedApp;
    private IUserRepository userRepository;
    private IRoleRepository roleRepository;

    @Before
    public void initTests() {
        roleRepository = new RoleRepository(new ConcurrentHashMap<>(), new ConcurrentHashMap<>(), new ConcurrentHashMap<>());
        userRepository = new UserRepository(new ConcurrentHashMap<>());
        roleBasedApp = new RoleBasedApplication(userRepository, roleRepository);
    }

    @Test
    public void OneUserOneRoleAllowTest() throws Exception {
        long currentTime = System.currentTimeMillis();
        User user = new User("user1@gmail.com");
        List<Statement> statementList = new ArrayList<>();
        statementList.add(new Statement(ActionType.READ, Resource.PAYMENT_DASHBOARD, AccessType.ALLOW));
        Role role = new Role("role1", 0, currentTime, currentTime, statementList);

        try {
            // user and role are not added yet.
            roleBasedApp.allow(user.getEmailId(), Resource.PAYMENT_DASHBOARD, ActionType.READ);
        } catch (InvalidInputException e) {
            System.out.println("this is working as expected since we have created user or role");
        }

        roleBasedApp.createRole(role);
        roleBasedApp.createUser(user);
        roleBasedApp.assignUserToRole(user.getEmailId(), role.getName());
        Assert.assertTrue(roleBasedApp.allow(user.getEmailId(), Resource.PAYMENT_DASHBOARD, ActionType.READ));
    }


    @Test
    public void OneUserOneRoleDenyTest() throws Exception {
        long currentTime = System.currentTimeMillis();
        User user = new User("user1@gmail.com");
        List<Statement> statementList1 = new ArrayList<>();
        statementList1.add(new Statement(ActionType.READ, Resource.PAYMENT_DASHBOARD, AccessType.DENY));
        Role role1 = new Role("role1", 0, currentTime, currentTime, statementList1);

        roleBasedApp.createRole(role1);
        roleBasedApp.createUser(user);
        roleBasedApp.assignUserToRole(user.getEmailId(), role1.getName());
        Assert.assertFalse(roleBasedApp.allow(user.getEmailId(), Resource.PAYMENT_DASHBOARD, ActionType.READ));
    }
}
