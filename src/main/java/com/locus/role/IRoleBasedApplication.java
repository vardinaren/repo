package com.locus.role;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.ActionType;
import com.locus.role.models.Resource;
import com.locus.role.models.Role;
import com.locus.role.models.User;


public interface IRoleBasedApplication {

    // assumption is, even if one Role allows user to access specific resource and actionType, we allow
    boolean allow(String userId, Resource resource, ActionType actionType) throws InvalidInputException;

    boolean assignUserToRole(String userId, String roleId) throws InvalidInputException;

    boolean updateRole(Role role, int version) throws InvalidInputException;

    boolean createRole(Role role) throws InvalidInputException;

    boolean createUser(User user) throws InvalidInputException;

    boolean removeUserFromRole(String userId, String roleId);
}
