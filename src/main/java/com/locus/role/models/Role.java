package com.locus.role.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;


@Data
@AllArgsConstructor
public class Role {
    private String name;
    private int version;
    private long createdAt;
    private long updatedAt;
    private List<Statement> statementList;
}
