package com.locus.role.models;


public enum ActionType {
    READ, WRITE, DELETE
}
