package com.locus.role.models;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class Statement {
    private ActionType actionType;
    private Resource resource;
    private AccessType accessType;
}
