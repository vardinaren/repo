package com.locus.role.models;


public enum Resource {
    // for simplicity, limited the resources to two.

    // If we have large number of resources, we can consider moving away from enums and use
    // configuration based approach with validations.
    //
    REPORT_DASHBOARD, PAYMENT_DASHBOARD
}
