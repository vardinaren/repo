package com.locus.role.models;


public enum AccessType {
    ALLOW, DENY
}
