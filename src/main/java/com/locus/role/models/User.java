package com.locus.role.models;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class User {
    // for simplicity, we are keeping only user id and email id.
    private String emailId;
}
