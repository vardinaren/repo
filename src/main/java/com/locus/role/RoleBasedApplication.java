package com.locus.role;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.*;
import com.locus.role.repository.IRoleRepository;
import com.locus.role.repository.IUserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


@Data
@AllArgsConstructor
public class RoleBasedApplication implements IRoleBasedApplication {
    private IUserRepository userRepository;
    private IRoleRepository roleRepository;

    @Override
    public boolean allow(String userId, Resource resource, ActionType actionType) throws InvalidInputException {
        List<Role> roleList = roleRepository.fetchRolesByUserId(userId);
        for (Role role : roleList) {
            for (Statement statement : role.getStatementList()) {
                Statement requestStatement = new Statement(actionType, resource, AccessType.ALLOW);
                if (statement.equals(requestStatement)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean assignUserToRole(String userId, String roleId) throws InvalidInputException {
        return roleRepository.assignRoleToUser(roleId, userId);
    }

    @Override
    public boolean updateRole(Role role, int version) throws InvalidInputException {
        role.setVersion(version);
        return roleRepository.updateRole(role);
    }

    @Override
    public boolean createRole(Role role) throws InvalidInputException {
        return roleRepository.updateRole(role);
    }

    @Override
    public boolean createUser(User user) throws InvalidInputException {
        return userRepository.createUser(user) && roleRepository.createUser(user);
    }

    @Override
    public boolean removeUserFromRole(String userId, String roleId) {
        return roleRepository.removeUserFromRole(userId, roleId);
    }
}
