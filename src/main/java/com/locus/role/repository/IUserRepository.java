package com.locus.role.repository;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.User;


public interface IUserRepository {
    User getUserByEmail(String email) throws InvalidInputException;

    boolean createUser(User user) throws InvalidInputException;

    boolean updateUser(User user, String email) throws InvalidInputException;
}
