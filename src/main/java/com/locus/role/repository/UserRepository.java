package com.locus.role.repository;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.User;

import java.util.Map;


public class UserRepository implements IUserRepository {
    private Map<String, User> emailToUserMap;

    public UserRepository(Map<String, User> emailToUserMap) {
        this.emailToUserMap = emailToUserMap;
    }

    @Override
    public User getUserByEmail(String email) throws InvalidInputException {
        if (emailToUserMap.containsKey(email)) {
            return emailToUserMap.get(email);
        }
        throw new InvalidInputException("Please check the email entered");
    }

    @Override
    public boolean createUser(User user) throws InvalidInputException {
        if (emailToUserMap.containsKey(user.getEmailId())) {
            throw new InvalidInputException("User already exists");
        }
        emailToUserMap.put(user.getEmailId(), user);
        return true;
    }

    @Override
    public boolean updateUser(User user, String email) throws InvalidInputException {
        if (emailToUserMap.containsKey(email)) {
            emailToUserMap.put(email, user);
        }
        throw new InvalidInputException("Please check the email entered");
    }
}
