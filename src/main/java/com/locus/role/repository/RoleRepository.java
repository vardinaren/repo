package com.locus.role.repository;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.Role;
import com.locus.role.models.User;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


public class RoleRepository implements IRoleRepository {
    // db access objects
    // to simplify, using Map
    private Map<String, Set<String>> userIdToRoleIdMap;
    private Map<String, Role> roleNameVersionToRole;
    private Map<String, Integer> roleNameToLastUpdatedVersion;

    private static final String ROLE_NAME_VERSION_KEY = "%s_%d";

    public RoleRepository(Map<String, Set<String>> userIdToRoleIdMap,
                          Map<String, Role> roleNameVersionToRole, Map<String, Integer> roleNameToLastUpdatedVersion) {
        this.userIdToRoleIdMap = userIdToRoleIdMap;
        this.roleNameVersionToRole = roleNameVersionToRole;
        this.roleNameToLastUpdatedVersion = roleNameToLastUpdatedVersion;
    }

    public Role fetchRole(String roleName, int version) throws InvalidInputException {
        String key = buildRoleNameVersionKey(roleName, version);
        if (roleNameVersionToRole.containsKey(key)) {
            return roleNameVersionToRole.get(key);
        }
        throw new InvalidInputException("Invalid Role Name provided!");
    }


    public List<Role> fetchRolesByUserId(String userId) throws InvalidInputException {
        if (userIdToRoleIdMap.containsKey(userId)) {
            Set<String> roles = userIdToRoleIdMap.get(userId);

            List<Role> respList = new ArrayList<Role>();
            for (String roleName : roles) {
                int version = roleNameToLastUpdatedVersion.get(roleName);
                Role role = fetchRole(roleName, version);
                respList.add(role);
            }
            return respList;
        }
        throw new InvalidInputException("Invalid User Id");
    }

    public boolean updateRole(Role role) throws InvalidInputException {
        if (roleNameToLastUpdatedVersion.getOrDefault(role.getName(), 0) == role.getVersion()) {
            AtomicInteger currentVersion = new AtomicInteger(role.getVersion());
            int newVersion = currentVersion.incrementAndGet();
            roleNameToLastUpdatedVersion.put(role.getName(), newVersion);
            String key = buildRoleNameVersionKey(role.getName(), newVersion);
            roleNameVersionToRole.put(key, role);

            return true;
        }
        throw new InvalidInputException("Version is outdated!");
    }

    @Override
    public boolean assignRoleToUser(String roleName, String userId) throws InvalidInputException {
        if (!roleNameToLastUpdatedVersion.containsKey(roleName) || !userIdToRoleIdMap.containsKey(userId)) {
            throw new InvalidInputException("Please check the role name and user id ");
        }
        Set<String> roles = userIdToRoleIdMap.getOrDefault(userId, new HashSet<>());
        roles.add(roleName);
        userIdToRoleIdMap.put(userId, roles);
        return true;
    }

    @Override
    public boolean createUser(User user) throws InvalidInputException {
        if (!userIdToRoleIdMap.containsKey(user.getEmailId())) {
            userIdToRoleIdMap.put(user.getEmailId(), new HashSet<>());
            return true;
        }
        throw new InvalidInputException("user already exists!");
    }

    @Override
    public boolean removeUserFromRole(String userId, String roleId) {
        userIdToRoleIdMap.getOrDefault(userId, new HashSet<>()).remove(roleId);
        return true;
    }

    private String buildRoleNameVersionKey(String roleName, int version) {
        return String.format(ROLE_NAME_VERSION_KEY, roleName, version);
    }
}
