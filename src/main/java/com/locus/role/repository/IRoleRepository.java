package com.locus.role.repository;

import com.locus.role.exception.InvalidInputException;
import com.locus.role.models.Role;
import com.locus.role.models.User;

import java.util.List;


public interface IRoleRepository {

    Role fetchRole(String roleName, int version) throws InvalidInputException;

    // userId refers to email id
    List<Role> fetchRolesByUserId(String userId) throws InvalidInputException;

    // if the role is not existing already, this will create one role otherwise it will update the role
    boolean updateRole(Role role) throws InvalidInputException;

    // we can only assign the updated version.
    boolean assignRoleToUser(String roleName, String userId) throws InvalidInputException;

    boolean createUser(User user) throws InvalidInputException;

    boolean removeUserFromRole(String userId, String roleId);
}

